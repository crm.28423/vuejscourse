const BASE_URL = process.env.VUE_APP_BASE_URL;

export default {
  async contactCoach(context, payload) {
    const newRequest = {
      // coachId: payload.coachId,
      userEmail: payload.email,
      message: payload.message
    };

    const response = await fetch(`${BASE_URL}requests/${payload.coachId}.json`, {
      method: 'POST',
      body: JSON.stringify(newRequest)
    });
    const responseData = await response.json();

    if (!response.ok) {
      throw new Error(responseData.message || 'Failed to send request');
    }

    console.log('responseData.name', responseData.name);

    newRequest.id = responseData.name;
    newRequest.coachId = payload.coachId;

    context.commit('addRequest', newRequest);
  },
  async loadRequests({ commit, rootGetters }) {
    const userId = rootGetters.userId
    const response = await fetch(`${BASE_URL}requests/${userId}.json`);
    const responseData = await await response.json();

    if (!response.ok) {
      throw new Error(responseData.message || 'Failed to fetch requests.');
    }

    const requests = [];

    for (const key in responseData) {
      const request = {
        id: key,
        coachId: userId,
        userEmail: responseData[key].userEmail,
        message: responseData[key].message
      };

      requests.push(request);
    }

    commit('setRequests', requests);
  }
};
