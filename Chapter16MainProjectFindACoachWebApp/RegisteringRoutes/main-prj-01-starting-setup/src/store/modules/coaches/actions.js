const BASE_URL = process.env.VUE_APP_BASE_URL;

export default {
  async registerCoach({ commit, rootGetters }, data) {
    const userId = rootGetters.userId;
    const coachData = {
      id: userId,
      firstName: data.first,
      lastName: data.last,
      description: data.desc,
      hourlyRate: data.rate,
      areas: data.areas,
    };

    const response = await fetch(`${BASE_URL}coaches/${userId}.json`, {
      method: 'PUT',
      body: JSON.stringify(coachData),
    });

    if (!response.ok) {
      // throw error
    } else {
      commit('registerCoach', {
        ...coachData,
        id: userId,
      });
    }
  },
  async loadCoaches({ commit, getters }, payload) {
    if (!payload.forceRefresh && !getters.shouldUpdate) {
      return;
    }

    const response = await fetch(`${BASE_URL}coaches.json`);
    const responseData = await response.json();

    if (!response.ok) {
      const error = new Error(responseData.message || 'Failed to fetch!');
      throw error;
    }

    const coaches = [];

    for (const key in responseData) {
      const coach = {
        id: key,
        firstName: responseData[key].firstName,
        lastName: responseData[key].lastName,
        description: responseData[key].description,
        hourlyRate: responseData[key].hourlyRate,
        areas: responseData[key].areas,
      };

      coaches.push(coach);
    }

    commit('setCoaches', coaches);
    commit('setFetchTimestamp');
  },
};
