How Vue Works

      	 ->Mutations
      	|	    |
      	|	    |
      	|	    V
    Actions	App-wide, central Data -----> Getters --
      	^	Store/State Store			|
      	|	     					|
      	|	     					|
      	|	     					|
      	|	     					|
      	 --Components<----------------------------------


Actions can use asynchronous code. And because of that, it's actually considered a good practice in general, to always put Actions between Components and Mutations, even though Components could commit Mutations themselves.