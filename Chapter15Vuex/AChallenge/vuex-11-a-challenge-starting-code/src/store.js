import { createStore } from 'vuex';

const products = [
  {
    id: 'p1',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Books_HD_%288314929977%29.jpg/640px-Books_HD_%288314929977%29.jpg',
    title: 'Book Collection',
    description: 'A collection of must-read books. All-time classics included!',
    price: 99.99,
  },
  {
    id: 'p2',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/d/df/Tent_with_Mountain.jpg',
    title: 'Mountain Tent',
    description: 'A tent for the ambitious outdoor tourist.',
    price: 129.99,
  },
  {
    id: 'p3',
    image:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Good_Food_Display_-_NCI_Visuals_Online.jpg/640px-Good_Food_Display_-_NCI_Visuals_Online.jpg',
    title: 'Food Box',
    description:
      'May be partially expired when it arrives but at least it is cheap!',
    price: 6.99,
  },
];

export const store = createStore({
  state() {
    return {
      isLoggedIn: false,
      products: [...products],
      cart: {
        items: [],
        total: 0,
        qty: 0,
      },
    };
  },
  mutations: {
    setAuth(state, payload) {
      state.isLoggedIn = payload.value;
    },
    setCartQty(state, payload) {
      state.cart.qty = payload.value;
    },
    setCartTotal(state, payload) {
      state.cart.total = payload.value;
    },
    deleteItemFromCart(state, { prodId }) {
      state.cart.items = state.cart.items.filter((ci) => ci.productId !== prodId)
    },
    incrementCartItemQty(state, { productIndex }) {
      state.cart.items[productIndex].qty = state.cart.items[productIndex].qty + 1;
    },
    addNewItemToCart(state, { item }) {
      state.cart.items = [item, ...state.cart.items];
    },
  },
  actions: {
    login(context) {
      context.commit('setAuth', { value: true });
    },
    logout(context) {
      context.commit('setAuth', { value: false });
    },
    addProductToCart({ getters, commit }, newProduct) {
      const productInCartIndex = getters.cart.items.findIndex(
        (ci) => ci.productId === newProduct.id
      );

      if (productInCartIndex >= 0) {
        commit('incrementCartItemQty', { productIndex: productInCartIndex });
      } else {
        const newItem = {
          productId: newProduct.id,
          title: newProduct.title,
          image: newProduct.image,
          price: newProduct.price,
          qty: 1,
        };

        commit('addNewItemToCart', { item: newItem });
      }
      const cartQuantity = getters.cart.qty + 1;
      const cartTotal = getters.cart.total + newProduct.price;

      commit('setCartQty', { value: cartQuantity });
      commit('setCartTotal', { value: cartTotal });
    },
    removeProductFromCart({ getters, commit }, { prodId }) {
      const productInCartIndex = getters.cart.items.findIndex(
        (ci) => ci.productId === prodId
      );

      const productToRemove = getters.cart.items[productInCartIndex];
      const cartQuantity = getters.cart.qty - productToRemove.qty;
      const cartTotal = getters.cart.total - (productToRemove.price * productToRemove.qty);

      commit('setCartQty', { value: cartQuantity });
      commit('setCartTotal', { value: cartTotal });
      commit('deleteItemFromCart', { prodId });
    },
  },
  getters: {
    isAuth(state) {
      return state.isLoggedIn;
    },
    products(state) {
      return state.products;
    },
    cart(state) {
      return state.cart;
    },
  },
});
