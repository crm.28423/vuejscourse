const app = Vue.createApp({
  data() {
    return {
      friends: [
        {
          id: 'manuel',
          name: 'Manuel Lorenz',
          phone: '345 345341 141',
          email: 'manuel@mail.com'
        },
        {
          id: 'julie',
          name: 'Julie Johns',
          phone: '387 23488 367',
          email: 'julie@mail.com'
        },
      ],
    }
  },
});

app.component('friend-contact', {
  template: `
  <li>
    <h2>{{ friend.name }}</h2>
    <button @click="toggleDetails"> {{isDetailsVisible ? 'Hide' : 'Show'}} Details</button>
    <ul v-if="isDetailsVisible">
      <li><strong>Phone:</strong>{{ friend.phone }}</li>
      <li><strong>Email:</strong>{{ friend.email }}</li>
    </ul>
  </li>`,
  data() {
    return {
      isDetailsVisible: false,
      friend: {
        id: 'julie',
        name: 'Julie Johns',
        phone: '387 23488 367',
        email: 'julie@mail.com'
      }
    }
  },
  methods: {
    toggleDetails() {
      this.isDetailsVisible = !this.isDetailsVisible;
    }
  }
});

app.mount('#app');