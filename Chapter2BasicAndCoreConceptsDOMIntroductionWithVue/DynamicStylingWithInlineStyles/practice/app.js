const app = Vue.createApp({
  data() {
    return {
      boxA: false,
      boxB: false
    }
  },
  methods: {
    handleBoxStyle(box) {
      if (box === 'A') {
        this.boxA = !this.boxA
      } else if (box === 'B') {
        this.boxB = !this.boxB
      }
    }
  },
  computed: {
    active() {
      return {active: this.boxA}
    }
  }
});

app.mount('#styling');