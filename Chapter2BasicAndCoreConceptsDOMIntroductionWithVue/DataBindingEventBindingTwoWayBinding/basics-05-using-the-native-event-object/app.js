const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '',
      lastName: '',
      firstLastName: '',
      age: '',
      access: ''
    };
  },
  watch: {
    // refers to the data property name and watches its changes; executes whenever name in data changes
    name(value, oldValue) {
      console.log('oldValue', oldValue);
      if (value === '') {
        this.firstLastName = '';
      } else {
        this.firstLastName = value + ' ' + this.lastName
      }
    },
    lastName(value, oldValue) {
      console.log('oldValue', oldValue);
      if (value === '') {
        this.firstLastName = '';
      } else {
        this.firstLastName = value + ' ' + this.name
      }
    },
    counter(value) {
      if (value > 50) {
        const self = this;

        const timeoutId = setTimeout(function() {
          self.counter = 0;
          clearTimeout(timeoutId);
        }, 1500);
        // this.counter = 0;
      }
    },
    age(value) {
      if (value === '') { this.access = ''}

      if (value >= 18) {
        this.access = 'you can access'
      } else {
        this.access = 'you are too young'
      }
    }
  },
  computed: {
    fullName() {
      console.log('Run outputFullName');
      if (this.name === '' || this.lastName === '') {
        return '';
      }

      return `${this.name} ${this.lastName}`;
    },
    ageCategory () {
      if (this.age === '') { return ''}

      if (this.age < 12) {
        return 'Child';
      } else if (this.age > 12 && this.age < 18) {
        return 'Teenager';
      } else if (this.age > 18 && this.age < 60) {
        return 'Adult';
      } else {
        return 'Old'
      }
    },
  },
  methods: {
    outputFullName() {
      console.log('Run outputFullName');
      if (this.name === '') {
        return '';
      }

      return `${this.name} LastName`;
    },
    setName(event) {
      this.name = event.target.value;
    },
    add(num) {
      this.counter = this.counter + num;
    },
    reduce(num) {
      this.counter = this.counter - num;
      // this.counter--;
    },
    resetInput() {
      this.name = '';
    },
  }
});

app.mount('#events');
