const app = Vue.createApp({
    data: function() {
        return {
            courseGoalA: 'Learn Vue',
            courseGoalB: '<h2>Master Vue and building amazing apps</h2>',
            vueLink: 'https://vuejs.org',
            googleLink: 'https://www.google.com/',
            time: Date.now(),
        }
    },
    methods: {
        outputGoal() {
            const randomNumber = Math.random();
            // this.outputTime();
            if(randomNumber < 0.5) {
                return this.courseGoalA
            } else {
                return this.courseGoalB
            }
        },
        outputTime() {
            setInterval(() => this.time = Date.now(), 1000);
        }
    }
});

app.mount('#user-goal');