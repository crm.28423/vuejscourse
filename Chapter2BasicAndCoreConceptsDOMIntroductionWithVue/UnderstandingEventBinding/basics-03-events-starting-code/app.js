const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      inputName: '',
      confirmedName: '',
      time: Date.now(),
      intervalId: null,
    };
  },
  methods: {
    add(num) {
      this.counter += num;
    },
    reduce(num) {
      this.counter -= num;
    },
    handleNameInput(event, prefixString) {
      if (event.target.value.length > 0) {
        this.inputName = `${prefixString} ${event.target.value}`;
      } else {
        this.inputName = '';
      }
    },
    submitForm(e) {
      alert('Submitted');
    },
    confirmInput() {
      this.confirmedName = this.inputName;
    },
    startTimer() {
      if (this.intervalId) return;
      this.intervalId = setInterval(() => this.time = Date.now(), 1000);
    },
    stopTimer() {
      clearInterval(this.intervalId);
      this.intervalId = null;
    }
  }
});

app.mount('#events');
