const app = Vue.createApp({
  data() {
    return {
      inputValue: '',
      commentValue: '',
      todos: [],
    };
  },
  methods: {
    addTodo() {
      if (this.inputValue.length > 0) {
        this.todos.push({todo: this.inputValue});
        this.inputValue = '';
      }
    },
    removeTodo(index) {
      this.todos.splice(index, 1);
    },
    addComment(value, index) {
      const todo = this.todos.find((_, idx) => idx === index);
      todo.comment = value;
      this.commentValue = '';
    }
  }
});

app.mount('#user-todos');
