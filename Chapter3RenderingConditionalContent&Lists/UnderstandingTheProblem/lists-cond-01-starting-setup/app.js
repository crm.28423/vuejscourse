const app = Vue.createApp({
  data() {
    return {
      inputValue: '',
      goals: [],
      dataObj: {
        name: 'John',
        lastName: 'Doe',
        age: 30
      }
    };
  },
  methods: {
    addGoal() {
      if (this.inputValue.length > 0) {
        this.goals.push(this.inputValue);
        this.inputValue = '';
        console.log('this.goals:', this.goals);
      }
    },
    removeGoal(index) {
      console.log('index:', index)
      this.goals.splice(index, 1)
    }
  }
});

app.mount('#user-goals');
